from django.conf.urls import url

from . import views

urlpatterns = [
	# /test/
	url(r'^$', views.home, name = 'home'),
	url(r'^register/$', views.register, name = 'register'),
	url(r'^login/$', views.login_account, name = 'login'),

	
	url(r'^activate/(?P<activation_key>[a-z0-9]{10})/$', views.activate, name='activate'),
	url(r'^username/(?P<username>[a-z]+)/account/$', views.challenge, name='challenge'),

	url(r'^username/(?P<username>[a-z]+)/profile/$', views.complete, name = 'complete'),
	url(r'^username/(?P<username>[a-z]+)/read/$', views.read, name = 'read'),
	
	url(r'^username/(?P<username>[a-z]+)/write/$', views.write, name = 'write'),

	url(r'^(?P<status>[a-z]+)/(?P<username>[a-z]+)/(?P<id>[0-9]+)/$', views.like, name = 'like'),
	
	url(r'^username/(?P<username>[a-z]+)/show/$', views.show_profile, name = 'show'),

]
