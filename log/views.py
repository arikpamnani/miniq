from django.shortcuts import render
from django.http import HttpResponse, HttpResponseForbidden
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

from django.core.mail import send_mail, EmailMessage

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.conf import settings

from .models import Profile, Information, Blog
from .forms import RegisterForm, VerificationForm

from django.template import RequestContext

import random
import hashlib
import datetime
import json

from django.utils import timezone


def home(request):
	return render(request, 'index.html')

def randomhash(string):
	salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
	res = hashlib.sha1(salt + string)
	return res.hexdigest()[:10]

def activate(request, activation_key = None):
	if activation_key is None:
		return HttpResponseForbidden()

	else:
		obj = Profile.objects.get(activation_key = activation_key)
		user = User.objects.get(id = obj.user_link_id)
		
		user.is_active = True
		user.save()
		request.session["errors"] = "Your account is now activated. You may now login to access your account."
		return redirect('/test/login')
	

def login_account(request):
	if request.method == "POST":
		request.session["errors"] = ""
		form = VerificationForm(request.POST)
		color = ""
		
		if form.is_valid():
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			
			user = authenticate(username = username, password = password)	
			
			
			if user is not None:				
				

				if user.is_staff:
					return redirect('/admin/')
				login(request, user)
		
				request.session["user_username"] = request.user.username
				request.session["user_email"] = request.user.email
				request.session["user_id"] = request.user.id
				request.session["errors"] = ""			
		
				return redirect('/test/username/' + request.session["user_username"] + "/account/")
			else:
				try:
					User.objects.get(username = username)
					request.session["errors"] = "Your account is not activated."
					color = "red"
				except:
					request.session["errors"] = "The username or password you entered is incorrect."
					color = "red"
					
	else:
		form = VerificationForm()
		request.session["errors"] = ""
		color = ""	
	
	return render(request, 'login.html', {'login_errors':request.session["errors"], 'color':color})


@login_required(redirect_field_name=None, login_url = "/test/login/")
def logout_account(request):
	request.session.flush()
	logout(request)
	request.session["errors"] = "You have successfully logged out."
	return redirect('/test/')

def register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		errors = ""	
		color = "teal"		
		if form.is_valid():

			email_id = form.cleaned_data["email"]
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]

			valid = checkvalid(email_id)
			if not valid:
				errors = "An account already exists for " + email_id
				color = "red"
		
			else:
				try:	
					User.objects.create_user(username, email_id, password, is_active = False)
					user = User.objects.get(email = email_id)
					obj = Profile()
					obj.user_link_id = user.id					
					obj.activation_key = randomhash(username)
					obj.save()
				
					info = Information()
					info.user_link_id = user.id
					info.username = username
					info.save()

					errors = "Your account is created. Click on the link sent to your E-mail to activate the account."		
					# send_mail(subject = "Account confirmation", from_email = settings.EMAIL_HOST_USER, recipient_list=[email_id], fail_silently=False, html_message = """<html>Click on 127.0.0.1:8000/test/activate/ to activate your account.</html>""")

				except:
					errors = "Username already in use. Use a different one."
					color = "red"	
				
	else:
		form = RegisterForm()
		errors = ""
		color = ""

	return render(request, 'signup.html', {'form':form, 'errors':errors, 'color':color})

@login_required(login_url = "/test/login/", redirect_field_name=None)
def complete(request, username = ''):
	if username == request.session["user_username"]:
		obj = Information.objects.get(username = request.session["user_username"])
		display = dict()
		display["username"] = request.session["user_username"]
		display["dob"] = obj.dob
		display["topic"] = obj.topic
		display["about"] = obj.about
		display["occupation"] = obj.occupation
		display["organisation"] = obj.organisation
	
		if request.method == 'POST':
			username = request.session["user_username"]
			info = Information.objects.get(username = username)

			info.dob = request.POST.get("dob")
			info.topic = request.POST.get("topic")
			info.about = request.POST.get("about")
			info.occupation = request.POST.get("occupation")
			info.organisation = request.POST.get("organisation")

			info.save()
			return render(request, 'account.html', {'username':username})

		return render(request, 'profile.html', display)

	else:
		return HttpResponseForbidden()
	



@login_required(login_url = "/test/login/", redirect_field_name = None)
def challenge(request, username = ''):	
	return render(request, 'account.html', {'username':username})


@login_required(login_url = "/test/login/", redirect_field_name = None)
def read(request, username = ''):
	if username == request.session["user_username"]:
		username = request.session["user_username"]
		user = User.objects.get(username = username)
		blogs = Blog.objects.order_by("date")
		count = len(blogs) 
	
		return render(request, 'read.html', {'blogs':blogs, 'count':count, 'username':username})

	else:
		return HttpResponseForbidden()

@login_required(login_url = "/test/login/", redirect_field_name = None)
def write(request, username = ''):
	if username == request.session["user_username"]:
	
		user = User.objects.get(username = username)
		info = Information.objects.get(user_link_id = user.id)
		if request.method == 'POST':
			content = request.POST.get("content")
			label = request.POST.get("label")
			blog = Blog()

			blog.author = username
			blog.content = content
			blog.label = label
			blog.date = timezone.now()
			blog.save()

			info.blogs_written.add(blog)
			info.save()

			return redirect('/test/username/' + username + '/account/')
		
		return render(request, 'write.html', {'username':username})
	else:
		return HttpResponseForbidden()

@login_required(login_url = "/test/login/", redirect_field_name = None)
def like(request, status = '', username = '', id = ''):
	if username == request.session["user_username"]:
		
		username = request.session["user_username"]
		user = User.objects.get(username = username)
		blog = Blog.objects.get(id = id)
	
		blogs = Blog.objects.order_by("date")
		count = len(blogs) 

		if status == "like":
			blog.user_likes.add(user)
			blog.upvotes += 1
			
		
		elif status == "unlike":
			blog.user_likes.remove(user)
			blog.upvotes -= 1
			
		blog.save()
		return HttpResponse(blog.id)

	else:
		return HttpResponseForbidden()

@login_required(login_url = "/test/login/", redirect_field_name = None)
def show_profile(request, username = ''):

	current_user = User.objects.get(username = request.session["user_username"])
	user = User.objects.get(username = username)
	editable = False

	if user == current_user:
		editable = True
	
	info = Information.objects.get(user_link_id = user.id)
	blogs_written = info.blogs_written.all()

	return render(request, 'show.html', {'info':info, 'user':user, 'blogs_written':blogs_written, 'editable':editable, 'username':username})
		

def checkvalid(email_id):
	try:
		s = User.objects.get(email = email_id)
		return False
	except:
		return True
		
	



	
	
	
		
		
		
