from django.contrib import admin

# Register your models here.

from .models import Profile, Information, Blog

class ProfileAdmin(admin.ModelAdmin):
	list_display = ("id", "activation_key", "user_link_id")


admin.site.register(Blog)
admin.site.register(Profile)
admin.site.register(Information)
