# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2016-07-14 18:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('log', '0005_auto_20160714_1838'),
    ]

    operations = [
        migrations.AddField(
            model_name='information',
            name='about',
            field=models.CharField(max_length=300, null=True),
        ),
    ]
